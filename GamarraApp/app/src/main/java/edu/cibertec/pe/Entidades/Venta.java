package edu.cibertec.pe.Entidades;

import java.util.Date;

public class Venta {

    private int id;
    private int tienda_id;
    private int usuario_id;
    private double prec_total;
    private Date fec_emision;
    private int estado_id;
    private Date fec_cancelacion;
    private String observacion;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getTienda_id() {
        return tienda_id;
    }

    public void setTienda_id(int tienda_id) {
        this.tienda_id = tienda_id;
    }

    public int getUsuario_id() {
        return usuario_id;
    }

    public void setUsuario_id(int usuario_id) {
        this.usuario_id = usuario_id;
    }

    public double getPrec_total() {
        return prec_total;
    }

    public void setPrec_total(double prec_total) {
        this.prec_total = prec_total;
    }

    public Date getFec_emision() {
        return fec_emision;
    }

    public void setFec_emision(Date fec_emision) {
        this.fec_emision = fec_emision;
    }

    public int getEstado_id() {
        return estado_id;
    }

    public void setEstado_id(int estado_id) {
        this.estado_id = estado_id;
    }

    public Date getFec_cancelacion() {
        return fec_cancelacion;
    }

    public void setFec_cancelacion(Date fec_cancelacion) {
        this.fec_cancelacion = fec_cancelacion;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }
}
