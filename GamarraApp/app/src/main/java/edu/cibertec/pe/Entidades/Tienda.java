package edu.cibertec.pe.Entidades;

public class Tienda {

    private int id;
    private int galeria_id;
    private String raz_soc;
    private String ruc;
    private String direccion;
    private String telefono;
    private String logo_path;

    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public int getGaleria_id() {
        return galeria_id;
    }
    public void setGaleria_id(int galeria_id) {
        this.galeria_id = galeria_id;
    }
    public String getRaz_soc() {
        return raz_soc;
    }
    public void setRaz_soc(String raz_soc) {
        this.raz_soc = raz_soc;
    }
    public String getRuc() {
        return ruc;
    }
    public void setRuc(String ruc) {
        this.ruc = ruc;
    }
    public String getDireccion() {
        return direccion;
    }
    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }
    public String getTelefono() {
        return telefono;
    }
    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }
    public String getLogo_path() {
        return logo_path;
    }
    public void setLogo_path(String logo_path) {
        this.logo_path = logo_path;
    }
}
