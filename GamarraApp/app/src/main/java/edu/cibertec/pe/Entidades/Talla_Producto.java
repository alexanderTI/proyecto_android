package edu.cibertec.pe.Entidades;

public class Talla_Producto {

    private int producto_id;
    private int talla_id;

    public int getProducto_id() {
        return producto_id;
    }

    public void setProducto_id(int producto_id) {
        this.producto_id = producto_id;
    }

    public int getTalla_id() {
        return talla_id;
    }

    public void setTalla_id(int talla_id) {
        this.talla_id = talla_id;
    }
}
