package edu.cibertec.pe;

import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import me.riddhimanadib.library.BottomBarHolderActivity;
import me.riddhimanadib.library.NavigationPage;

public class MainActivity extends BottomBarHolderActivity implements InicioActivity.OnFragmentInteractionListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        NavigationPage page1 = new NavigationPage("Inicio", ContextCompat.getDrawable(this, R.drawable.ic_home_black_24dp), InicioActivity.newInstance());
        NavigationPage page2 = new NavigationPage("Buscar", ContextCompat.getDrawable(this, R.drawable.ic_search_black_24dp), CategoriaActivity.newInstance());
        NavigationPage page3 = new NavigationPage("Cuenta", ContextCompat.getDrawable(this, R.drawable.ic_account_circle_black_24dp), CuentaActivity.newInstance());
        NavigationPage page4 = new NavigationPage("Mas", ContextCompat.getDrawable(this, R.drawable.ic_more_horiz_black_24dp), GaleryaActivity.newInstance());

        List<NavigationPage> navigationPages = new ArrayList<>();
        navigationPages.add(page1);
        navigationPages.add(page2);
        navigationPages.add(page3);
        navigationPages.add(page4);

        super.setupBottomBarHolderActivity(navigationPages);
    }

}
