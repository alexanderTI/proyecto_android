package edu.cibertec.pe.Persistencia;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class AdminSQLiteOpenHelper extends SQLiteOpenHelper {

    private static final String TABLA_GALERIA = "CREATE TABLE galeria" +
            "(_id integer primary key autoincrement, nombre TEXT,direccion TEXT)";

    private static final String TABLA_TIENDA = "CREATE TABLE tienda" +
            "(_id integer primary key autoincrement, galeria_id integer,razon_social TEXT, ruc TEXT, " +
            "direccion TEXT,telefono TEXT,logo_path TEXT )";

    private static final String TABLA_ALMACEN = "CREATE TABLE almacen" +
            "(_id integer primary key autoincrement, tienda_id integer,direccion TEXT)";

    private static final String TABLA_COLOR = "CREATE TABLE color" +
            "(_id integer primary key autoincrement, detalle TEXT)";

    private static final String TABLA_TALLA = "CREATE TABLE talla" +
            "(_id integer primary key autoincrement, detalle TEXT)";

    private static final String TABLA_CATEGORIA = "CREATE TABLE categoria" +
            "(_id integer primary key autoincrement, detalle TEXT)";

    private static final String TABLA_PRODUCTO = "CREATE TABLE producto" +
            "(_id integer primary key autoincrement, tienda_id integer,almacen_id integer,categoria_id integer," +
            "nombre TEXT, uni_ned TEXT, imagen_path TEXT, stock integer, precio decimal)";

    private static final String TABLA_COLOR_PRODUCTO = "CREATE TABLE color_producto" +
            "(producto_id integer primary key, color_id primary key)";

    private static final String TABLA_TALLA_PRODUCTO = "CREATE TABLE talla_producto" +
            "(producto_id integer primary key,talla_id integer primary key)";

    private static final String TABLA_USUARIO = "CREATE TABLE usuario" +
            "(_id integer primary key autoincrement,nickname TEXT, pasword TEXT, fecha_registro DATE)";

    private static final String TABLA_ESTADO = "CREATE TABLE estado" +
            "(_id integer primary key autoincrement, detalle TEXT)";

    private static final String TABLA_VENTA = "CREATE TABLE venta" +
            "(_id integer primary key autoincrement, tienda_id integer,usuario_id integer, precio_total decimal," +
            "fecha_emicion DATETIME, estado_id integer, fecha_cancelacion DATETIME, observacion TEXT)";

    private static final String TABLA_DETALLE_VENTA = "CREATE TABLE detalle_venta" +
            "(_id integer primary key autoincrement, producto_id integer,cantidad integer,precio_vemtga decimal)";

    private static final String TABLA_CONSTANTE = "CREATE TABLE constante" +
            "(_id integer primary key autoincrement, detalle TEXT)";


    public AdminSQLiteOpenHelper(Context context, String nombre, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, nombre, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(TABLA_GALERIA);
        db.execSQL(TABLA_TIENDA);
        db.execSQL(TABLA_ALMACEN);
        db.execSQL(TABLA_COLOR);
        db.execSQL(TABLA_TALLA);
        db.execSQL(TABLA_CATEGORIA);
        db.execSQL(TABLA_PRODUCTO);
        db.execSQL(TABLA_COLOR_PRODUCTO);
        db.execSQL(TABLA_TALLA_PRODUCTO);
        db.execSQL(TABLA_USUARIO);
        db.execSQL(TABLA_ESTADO);
        db.execSQL(TABLA_VENTA);
        db.execSQL(TABLA_DETALLE_VENTA);
        db.execSQL(TABLA_CONSTANTE);
    }


    @Override
    public void onUpgrade(SQLiteDatabase db, int versionAnte, int versionNue) {
        db.execSQL("drop table if exists TABLA_GALERIA");
        db.execSQL("drop table if exists TABLA_TIENDA");
        db.execSQL("drop table if exists TABLA_ALMACEN");
        db.execSQL("drop table if exists TABLA_COLOR");
        db.execSQL("drop table if exists TABLA_TALLA");
        db.execSQL("drop table if exists TABLA_CATEGORIA");
        db.execSQL("drop table if exists TABLA_PRODUCTO");
        db.execSQL("drop table if exists TABLA_COLOR_PRODUCTO");
        db.execSQL("drop table if exists TABLA_TALLA_PRODUCTO");
        db.execSQL("drop table if exists TABLA_USUARIO");
        db.execSQL("drop table if exists TABLA_ESTADO");
        db.execSQL("drop table if exists TABLA_VENTA");
        db.execSQL("drop table if exists TABLA_DETALLE_VENTA");
        db.execSQL("drop table if exists TABLA_CONSTANTE");

        onCreate(db);
    }
}
