package edu.cibertec.pe.Entidades;

public class Almacen {

    private int id;
    private int tienda_id;
    private String direccion;

    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public int getTienda_id() {
        return tienda_id;
    }
    public void setTienda_id(int tienda_id) {
        this.tienda_id = tienda_id;
    }
    public String getDireccion() {
        return direccion;
    }
    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }
}
