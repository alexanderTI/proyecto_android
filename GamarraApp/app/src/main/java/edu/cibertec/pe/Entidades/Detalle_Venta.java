package edu.cibertec.pe.Entidades;

public class Detalle_Venta {
    private int id;
    private int producto_id;
    private int cantidad;
    private double prec_venta;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getProducto_id() {
        return producto_id;
    }

    public void setProducto_id(int producto_id) {
        this.producto_id = producto_id;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public double getPrec_venta() {
        return prec_venta;
    }

    public void setPrec_venta(double prec_venta) {
        this.prec_venta = prec_venta;
    }
}
